# Programmation Orientée Objet
Un projet pour apprendre la POO avec JS

## Exercices

### Exo Person [point d'entrée](src/index.js)/[classe Person](src/Person.js)
1. Faire une nouvelle méthode greeting() dans la classe Person qui va renvoyer une chaîne de caractère avec "Hello, my name is ...., I am ... years old". Puis appeler cette méthode dans le index et en faire un console.log par exemple
2. Faire une méthode toHTML() dans la classe Person qui va créer un élément html (peu importe quoi) puis mettre en textContent le nom et prénom de la person puis renvoyer cet élément. Appeler la méthode et faire un append du toHTML dans le body par exemple
3. Faire que quand on click sur l'élément créé par le toHTML(), ça fasse une alert avec le résultat de la méthode greetings() à l'intérieur


### Exo TodoList OOP [point d'entrée](src/todo-oop.js) / [Task](src/todo/Task.js) / [TodoList](src/todo/TodoList.js)
1. Créer une classe Task qui ressemblera à ça (2 propriété, une string et un boolean, et une méthode) 

![task class](diagrammes/todo-task.png)

2. La méthode toggleDone fera en sorte de passer la valeur de la propriété done de true à false ou de false à true
3. Créer une classe TodoList qui aura une propriété tasks qui contiendra un tableau de Task, ainsi qu'une méthode addTask() et une méthode clearDone() 

![TodoList class](diagrammes/todo-list.png)

4. La méthode addTask va attendre une chaîne de caractère en argument et s'en servir pour faire une nouvelle instance de Task et l'ajouter dans le tableau tasks de la TodoList
5. La méthode clearDone() aura pour objectif de parcourir les différentes Tasks de la TodoList et de supprimer celles qui ont la propriété done à true
#### Affichage de la TodoList OOP
1. Dans la class Task, rajouter une méthode toHTML qui va utiliser le createElement pour générer les élément HTML d'une Task. Aide :  En l'occurrence, on veut que ça soit par exemple un li avec le label de la task dedans et un input type checkbox qui sera checked ou non selon si la task est done ou non, et il faudra ensuite return l'élément créé pour pouvoir l'append où on veut
2. Maintenant qu'on a une Task en HTML, le problème c'est que le HTML n'est pas vraiment lié aux données de la Task, c'est à dire que si on coche ou décoche la checkbox, ça change pas la valeur de la propriété done. Il faut donc faire en sorte que ça la modifie. Aide:  Dans le toHTML, on peut rajouter un event listener sur la checkbox, et faire que ça déclenche le toggleDone par exemple (pas hésiter à tester en mettant un ptit console log de this dans l'event listener) 
3. Dans la class TodoList, on va également rajouter une méthode toHTML qui aura pour objectif de générer le HTML de la TodoList même, mais aussi de toutes les tasks qu'elle contient. Aide:  la structure va être la même que pour la Task, avec create element puis return, mais entre les deux, il va falloir boucler sur chaque task pour générer leur HTML avec la méthode faite à l'étape 1 
4. Dans le HTML, on rajoute un ptit formulaire ainsi qu'un bouton Clear, et côté point d'entrée js, on rajoute des event listeners dessus pour qu'ils déclenchent la méthode addTask et la méthode clearDone respectivement, en regénérant le HTML de la TodoList après coup