import { Counter } from './Counter';
import {Person} from './Person';

let person1 = new Person('Demel', 'Jean', 30);
let person2 = new Person('Watson', 'Debby', 25);

console.log(person1.greeting());

const htmlPerson1 = person1.toHTML();

document.body.appendChild(htmlPerson1);
document.body.appendChild(person2.toHTML());


let counter1 = new Counter();
let counter2 = new Counter();

counter1.increment();
counter1.increment();
counter2.decrement();
counter2.reset();

console.log(counter1.value, counter2.value);
