
import { TodoList } from "./todo/TodoList";


const sectionTodo = document.querySelector('#todo');
let todoList = new TodoList();

let todoHTML = todoList.toHTML();
sectionTodo.appendChild(todoHTML);


const input = document.querySelector('#task');
const form = document.querySelector('form');
const clearBtn = document.querySelector('#clear');

form.addEventListener('submit', (event) => {
    event.preventDefault();

    todoList.addTask(input.value);
    
    refreshDisplay();
});

clearBtn.addEventListener('click', () => {
    todoList.clearDone();
    
    refreshDisplay();
});

/**
 * Fonction qui clean le html et qui recharge l'affichage de la todolist
 */
function refreshDisplay() {
    todoHTML.remove();
    todoHTML = todoList.toHTML();
    sectionTodo.appendChild(todoHTML);
}
// todoList.addTask('bloup');
// todoHTML.remove();

// let todoHTML2 = todoList.toHTML();
// document.body.appendChild(todoHTML2);

// todoList.addTask('bloup');

// todoHTML2.remove();
// document.body.appendChild(todoList.toHTML());
