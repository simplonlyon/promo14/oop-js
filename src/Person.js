export class Person {
    name;
    firstName;
    age;
    /**
     * 
     * @param {string} paramName 
     * @param {string} paramFirstName 
     * @param {number} paramAge 
     */
    constructor(paramName, paramFirstName, paramAge) {
        this.name = paramName;
        this.firstName = paramFirstName;
        this.age = paramAge;
    }
    /**
     * 
     * @returns {string}
     */
    greeting() {
        return `Hello my name is ${this.firstName} ${this.name}, I'm ${this.age} years old`;
        //return "Hello my name is "+this.firstName;
    }

    toHTML() {

        const element = document.createElement('div');
        element.textContent = this.name + ' ' + this.firstName;

        element.addEventListener('click', () => {
            alert(this.greeting());
        });

        return element;
    }
}
